from django.shortcuts import render, HttpResponse
from .models import *

# Create your views here.
def index(request):

    grupo_1 = Estudiante.objects.filter(grupo=1)
    grupo_4 = Estudiante.objects.filter(grupo=4)
    landaverde = Estudiante.objects.filter(apellidos='Landaverde')
    edad_20 = Estudiante.objects.filter(edad=20)
    edad_22_grupo_3 = Estudiante.objects.filter(grupo=3,edad=22)
    todos = Estudiante.objects.all()

    return render(request,'index.html',{'grupo_1':grupo_1,
                                        'grupo_4':grupo_4,
                                        'landaverde':landaverde,
                                        'edad_20':edad_20,
                                        'edad_22_grupo_3':edad_22_grupo_3,
                                        'todos':todos})